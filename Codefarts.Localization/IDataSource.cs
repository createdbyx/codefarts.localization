// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Localization
{
    /// <summary>
    /// Provides a date source interface.
    /// </summary>
    public interface IDataSource
    {
        /// <summary>Gets the localized string.</summary>
        /// <param name="key">The key for the localized string.</param>
        /// <returns>The value of the localized string.</returns>
        string GetString(string key);
    }
}