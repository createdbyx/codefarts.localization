﻿namespace Codefarts.Localization.Unity.Editor
{
    using Codefarts.AppCore.Interfaces;
    using Codefarts.IoC;
    using UnityEditor;

    [InitializeOnLoad]
    public class EditorInitialization
    {
        static UnityLocalizationProvider provider;

        /// <summary>
        /// Initializes static members of the <see cref="EditorInitialization"/> class.
        /// </summary>
        static EditorInitialization()
        {
            provider = new UnityLocalizationProvider();
            Container.Default.Register<ILocalizationProvider>(() => provider);
        }
    }
}