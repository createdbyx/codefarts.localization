// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Localization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Provides a <see cref="IDataSource"/> implementation for xml file based localization data.
    /// </summary>
    public class XDocumentLinqFileDataSource : IDataSource
    {
        /// <summary>Initializes a new instance of the <see cref="XDocumentLinqFileDataSource"/> class.</summary>
        /// <param name="languagesFolder">The languages folder where the localization files exist.</param>
        /// <exception cref="System.ArgumentNullException">If the languagesFolder parameter is null.</exception>
        public XDocumentLinqFileDataSource(string languagesFolder)
        {
            if (string.IsNullOrEmpty(languagesFolder)) throw new ArgumentNullException("languagesFolder");
            this.LanguagesFolder = languagesFolder;
        }

        /// <summary>Gets the languages folder.</summary>
        /// <value>The languages folder.</value>
        public string LanguagesFolder { get; private set; }

        /// <summary>The last write time of the file that was read.</summary>
        private DateTime lastWriteTime = DateTime.MinValue;

        /// <summary>Stores all the localized strings.</summary>
        private IDictionary<string, string> dataStore = new Dictionary<string, string>();

        /// <summary>Reads the localization file.</summary>
        /// <exception cref="System.IO.FileNotFoundException">Could not find language file.</exception>
        /// <remarks>Only reads the localization file if the file has been written to since it was last read.</remarks>
        private void Read()
        {
            var file = System.IO.Path.Combine(this.LanguagesFolder, System.Globalization.CultureInfo.CurrentCulture.Name);
            file = System.IO.Path.ChangeExtension(file, ".xml");
            if (!System.IO.File.Exists(file)) throw new System.IO.FileNotFoundException("Could not find language file.", file);

            var writeTime = System.IO.File.GetLastWriteTime(file);
            if (writeTime <= lastWriteTime)
            {
                return;
            }

            this.dataStore = ReadData(System.IO.File.OpenRead(file));
            this.lastWriteTime = writeTime;
        }

        /// <summary>Reads the localization data from a stream.</summary>
        /// <param name="stream">The stream to read the localization data from.</param>
        /// <returns>A <see cref="IDictionary{TKey,TValue}"/> of string key and value pairs.</returns>
        public static IDictionary<string, string> ReadData(System.IO.Stream stream)
        {
            var doc = XDocument.Load(stream);
            if (doc.Root == null)
            {
                return new Dictionary<string, string>();
            }

            var results = from x in doc.Root.Elements("entry")
                          where x.HasAttributes
                          let key = x.Attribute("key")
                          where key != null && !string.IsNullOrEmpty(key.Value)
                          select new { Key = key.Value, x.Value };
            return results.ToDictionary(k => k.Key, v => v.Value);
        }

        /// <summary>Gets the localized string.</summary>
        /// <param name="key">The key for the localized string.</param>
        /// <returns>The value of the localized string.</returns>
        public string GetString(string key)
        {
            this.Read();
            return this.dataStore[key];
        }
    }
}