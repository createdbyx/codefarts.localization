// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Localization.XDocuemnt
{
    using System.Globalization;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// Provides helper methods.
    /// </summary>
    public class LocalizationHelpers
    {
        /// <summary>Registers the XML from a manifest.</summary>
        /// <param name="assembly">The assembly in which the manifest exists.</param>
        /// <param name="manifestFilter">The manifest search filter.</param>
        public static void RegisterXmlFromManifest(Assembly assembly, string manifestFilter)
        {
            foreach (var name in assembly.GetManifestResourceNames())
            {
                if (!name.EndsWith(manifestFilter))
                {
                    continue;
                }

                var stream = assembly.GetManifestResourceStream(name);
                if (stream == null)
                {
                    continue;
                }

                var textStreamReader = new StreamReader(stream);
                var data = XDocumentLinqFileDataSource.ReadData(textStreamReader.BaseStream);

                var culture = Path.GetFileNameWithoutExtension(name.Replace(manifestFilter, string.Empty));
                LocalizationManager.Instance.Register(new CultureInfo(culture), data);
            }
        }
    }
}