﻿namespace Codefarts.Localization.Unity 
{
    using Codefarts.AppCore.Interfaces;
    using Codefarts.AppCore.Unity;
    using Codefarts.IoC;

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using UnityEngine;

    public class UnityLocalizationProvider : ILocalizationProvider
    {
        private IDictionary<string, string> localizationStrings;

        public UnityLocalizationProvider()
        {
            // only register current culture data
            var currentCulture = CultureInfo.CurrentCulture;
            var settings = Container.Default.Resolve<ISettingsProvider>(null);  

            // get centralized resources path 
            var resourcePath = settings == null ? "Codefarts.Unity" : settings.GetSetting(Constants.ResourceFolderKey, "Codefarts.Unity");

            // ensure default culture is en-us
            var defaultCulture = CultureInfo.GetCultureInfo("en-us");

            // get localization path for default culture
            var localPath = Path.Combine(resourcePath, "Localization");
            localPath = Path.Combine(localPath, defaultCulture.Name).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            this.LoadLocalizationData(defaultCulture, localPath);

            // get localization path for current system culture
            localPath = Path.Combine(resourcePath, "Localization");
            localPath = Path.Combine(localPath, currentCulture.Name).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            this.LoadLocalizationData(currentCulture, localPath);   
        }

        /// <summary>
        /// Sets up and loads localized strings for the localization system.
        /// </summary>
        public void LoadLocalizationData(CultureInfo currentCulture, string path)
        {
            // attempt to read localization data for the current culture
            var data = Resources.LoadAll<TextAsset>(path);
            if (data == null || data.Length == 0)
            {
                Debug.LogWarning(string.Format("No localization file(s) found for '{0}'", currentCulture.Name));
                return;
            }

            var entries = new Dictionary<string, string>();
            foreach (var item in data)
            {
                try
                {
                    var lines = item.text.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var line in lines)
                    {
                        var index = line.IndexOf(" ", StringComparison.Ordinal);
                        if (index == -1)
                        {
                            continue;
                        }

                        var keyValue = line.Substring(0, index).Trim();
                        var value = line.Substring(index).Trim();
                        value = value.Replace(@"\r\n", "\r\n");
                        var prefix = string.Empty;
                        if (!entries.ContainsKey(prefix + keyValue))
                        {
                            entries.Add(prefix + keyValue, value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
            }

            // register localization entries
            this.Register(currentCulture, entries);
        }

        public string GetString(string key)
        {
            return this.localizationStrings[key];
        }

        /// <summary>Registers a dataset of localized strings from a <see cref="T:System.Collections.Generic.IDictionary`2"/> reference.</summary>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo"/> that the strings will be registered into.</param>
        /// <param name="entries">A <see cref="T:System.Collections.Generic.IDictionary`2"/> containing the localized strings to add.</param>
        /// <remarks>This method will override any existing key values.</remarks>
        public void Register(CultureInfo culture, IDictionary<string, string> entries)
        {
            this.Register(culture, entries, true);
        }

        /// <summary>Registers a dataset of localized strings from a <see cref="T:System.Collections.Generic.IDictionary`2"/> reference.</summary>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo"/> that the strings will be registered into.</param>
        /// <param name="entries">A <see cref="T:System.Collections.Generic.IDictionary`2"/> containing the localized strings to add.</param>
        /// <param name="replace">If true existing key values will be replaced. Otherwise the existing values will be left as they are.</param>
        public void Register(CultureInfo culture, IDictionary<string, string> entries, bool replace)
        {
            foreach (var entry in entries)
            {
                if (replace)
                {
                    this.localizationStrings[entry.Key] = entry.Value;
                }
                else
                {
                    if (!this.localizationStrings.ContainsKey(entry.Key))
                    {
                        this.localizationStrings.Add(entry.Key, entry.Value);
                    }
                }
            }
        }
    }
}
